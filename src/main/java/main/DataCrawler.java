package main;

import org.apache.commons.httpclient.NoHttpResponseException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.lib.MultipleSequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaNewHadoopRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;

import java.io.*;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.*;

public class DataCrawler {

    private static final Logger logger = LogManager.getLogger(DataCrawler.class);

    private static final String INPUT_PATH = "/home/andrey/projects/interview/doubledata/src/main/resources/input";
    private static final String OUTPUT_PATH = "/home/andrey/projects/interview/doubledata/src/main/resources/output";

    private static final int MAX_RESULTS_PER_PART = 100;
    private static final int DOWNLOAD_BUFER = 4096;
    private static final int DOWNLOAD_RETRY_LIMIT = 5;
    private static final int DOWNLOAD_RETRY_WAIT = 500;

    private static String inputPath;
    private static String outputPath;

    public static void main(String[] args) {
        int fileCount = 0;
        List<String> filesToProcess;
        JavaSparkContext sc = createSparkContext();

        inputPath = args.length > 0 && args[0] != null ? args[0] : INPUT_PATH;
        outputPath = args.length > 1 && args[1] != null ? args[1] : OUTPUT_PATH;

        List<File> categoryDirectories = getSortedFileList(Paths.get(inputPath).toFile());

        do {
            filesToProcess = new ArrayList<>();
            int numberOfUrls = 0;

            for (File categoryDirectory : categoryDirectories) {
                List<File> files = getSortedFileList(categoryDirectory);
                if (files.size() > fileCount) {
                    File file = files.get(fileCount);
                    filesToProcess.add(file.getPath());
                    try {
                        numberOfUrls += countFileLines(file);
                    } catch (IOException e) {
                        logger.error(MessageFormat.format("Could not read file {0}", file.getPath()), e);
                    }
                }
            }
            fileCount++;
            if (!filesToProcess.isEmpty()) {
                processWithSpark(sc, String.join(",", filesToProcess), numberOfUrls / MAX_RESULTS_PER_PART + 1);
            }
        } while (!filesToProcess.isEmpty());
    }

    private static void processWithSpark(JavaSparkContext sc, String inputPath, int numberOfPartitions) {
        JavaPairRDD<LongWritable, Text> javaPairRDD = sc.newAPIHadoopFile(
                inputPath,
                TextInputFormat.class,
                LongWritable.class,
                Text.class,
                new Configuration()
        );
        JavaNewHadoopRDD<LongWritable, Text> hadoopRDD = (JavaNewHadoopRDD) javaPairRDD;

        JavaRDD<Tuple2<String, DownloadedObject>> namedLinesRDD = hadoopRDD.mapPartitionsWithInputSplit(
                (Function2<InputSplit, Iterator<Tuple2<LongWritable, Text>>, Iterator<Tuple2<String, DownloadedObject>>>) (inputSplit, lines) -> {
                    FileSplit fileSplit = (FileSplit) inputSplit;
                    Path path = fileSplit.getPath();
                    final String fileName = path.toUri().toString();
                    return new Iterator<Tuple2<String, DownloadedObject>>() {
                        @Override
                        public boolean hasNext() {
                            return lines.hasNext();
                        }

                        @Override
                        public Tuple2<String, DownloadedObject> next() {
                            Tuple2<LongWritable, Text> entry = lines.next();
                            String url = entry._2().toString();
                            String dataType = Paths.get(fileName).getParent().getFileName().toString();
                            return new Tuple2<>(dataType, new DownloadedObject(url, download(url), dataType));
                        }
                    };
                },
                true
        );

        namedLinesRDD.mapToPair(val -> new Tuple2<>(new Text(val._1()), val._2()))
                .repartition(numberOfPartitions)
                .saveAsHadoopFile(outputPath, Text.class, DownloadedObject.class, RDDMultipleTextOutputFormat.class);
    }

    private static List<File> getSortedFileList(File categoryDirectory) {
        File[] filesArray = categoryDirectory.listFiles();
        if (filesArray == null) {
            return Collections.emptyList();
        }
        List<File> files = Arrays.asList(filesArray);
        files.sort((file1, file2) -> file1.getName().compareTo(file2.getName()));
        return files;
    }

    private static int countFileLines(File file) throws IOException {
        LineNumberReader lineNumberReader;
        lineNumberReader = new LineNumberReader(new FileReader(file));
        lineNumberReader.skip(Long.MAX_VALUE);
        return lineNumberReader.getLineNumber();
    }

    private static JavaSparkContext createSparkContext() {
        SparkConf sparkConf = new SparkConf();

        sparkConf.setAppName("Spark crawler");
        sparkConf.setMaster("local[5]");
        sparkConf.set("spark.hadoop.validateOutputSpecs", "false");
        try {
            sparkConf.registerKryoClasses(new Class<?>[]{
                    Class.forName("org.apache.hadoop.io.Text")
            });
        } catch (ClassNotFoundException e) {
            logger.error("Could not register Kryo classes");
        }
        return new JavaSparkContext(sparkConf);
    }


    private static byte[] download(String url) {
        return download(url, 0);
    }

    private static byte[] download(String url, int retryCount) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = null;
        try {
            is = new URL(url).openStream();
            byte[] byteChunk = new byte[DOWNLOAD_BUFER];
            int n;

            while ((n = is.read(byteChunk)) > 0) {
                baos.write(byteChunk, 0, n);
            }
            return baos.toByteArray();
        } catch (SocketTimeoutException | NoHttpResponseException | UnknownHostException e) {
            if (retryCount <= DOWNLOAD_RETRY_LIMIT) {
                try {
                    int sleepMillis = DOWNLOAD_RETRY_WAIT * retryCount;
                    logger.info(MessageFormat.format("Could not download {0}, retry : {1} falling asleep for {2} ms", url, retryCount, sleepMillis));
                    Thread.sleep(sleepMillis);
                    return download(url, ++retryCount);
                } catch (InterruptedException interruptedException) {
                    logger.error(interruptedException);
                }
            } else {
                logger.error(MessageFormat.format("Could not download {0} after {1} retries", url, DOWNLOAD_RETRY_LIMIT), e);
            }
        } catch (MalformedURLException e) {
            logger.error(MessageFormat.format("Invalid download URL : {0}", url), e);
        } catch (IOException e) {
            logger.error(MessageFormat.format("Could not read bytes from URL : {0}", url), e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    logger.error(MessageFormat.format("Error while closing input stream during url download : {0}", url), e);
                }
            }
        }
        return new byte[]{};
    }

    private static class RDDMultipleTextOutputFormat<A, B> extends MultipleSequenceFileOutputFormat<A, B> {
        @Override
        protected String generateFileNameForKeyValue(A key, B value, String name) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            return Paths.get(outputPath)
                    .resolve(String.valueOf(key))
                    .resolve(String.valueOf(calendar.get(Calendar.YEAR)))
                    .resolve(String.valueOf(calendar.get(Calendar.MONTH)))
                    .resolve(String.valueOf(calendar.get(Calendar.DATE)))
                    .resolve(name)
                    .toString();
        }
    }

}
