package main;


import org.apache.hadoop.io.*;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

class DownloadedObject implements Serializable, Writable {
    private String url;
    private byte[] bytes;
    private Date downloadDate;
    private String type;
    private int size;

    public DownloadedObject() {
    }

    DownloadedObject(String url, byte[] bytes, String type) {
        this.url = url;
        this.bytes = bytes;
        this.downloadDate = new Date();
        this.type = type;
        if (bytes != null) {
            this.size = bytes.length;
        }
    }

    @Override
    public void write(DataOutput out) throws IOException {
        new Text(url).write(out);
        new BytesWritable(bytes).write(out);
        new LongWritable(downloadDate.getTime()).write(out);
        new Text(type).write(out);
        new IntWritable(size).write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        throw new UnsupportedOperationException();
    }
}
